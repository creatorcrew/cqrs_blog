<?php

use App\Infrastructure\Projection\Projector;
use App\Blog\Post;

require 'vendor/autoload.php';

$client    = new PDO('mysql:dbname=cqrs;host=127.0.0.1', 'laravel', '473eX10ioPlm,.');
$projector = Projector::instance();
$projector->register(
    [
        new \App\Infrastructure\Projection\PDO\PostWasCreatedProjection($client)
    ]
);

Post::writeNewFrom('Title', 'Content');
