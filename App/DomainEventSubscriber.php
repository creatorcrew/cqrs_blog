<?php


namespace App;


interface DomainEventSubscriber
{
    public function handle(DomainEvent $aDomainEvent);

    public function isSubscribedTo($aDomainEvent);
}
