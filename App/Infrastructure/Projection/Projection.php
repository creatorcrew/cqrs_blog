<?php


namespace App\Infrastructure\Projection;

use App\DomainEvent;

interface Projection
{
    public function listensTo();

    public function project(DomainEvent $event);
}
