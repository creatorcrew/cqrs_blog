<?php

namespace App\Infrastructure\Projection\PDO;


use App\DomainEvent;
use App\Infrastructure\Projection\Projection;
use App\Events\PostWasCreated;
use PDO;


class PostWasCreatedProjection implements Projection
{
    private PDO $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function listensTo(): string
    {
        return PostWasCreated::class;
    }


    public function project(DomainEvent $event): void
    {
        $stmt = $this->pdo->prepare(
            'INSERT INTO posts (post_id, title, content)
             VALUES (:post_id, :title, :content)'
        );

        $stmt->execute(
            [
                ':post_id' => $event->id()->uuid(),
                ':title'   => $event->title(),
                ':content' => $event->content(),
            ]
        );
    }


}
