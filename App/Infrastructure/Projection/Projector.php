<?php

namespace App\Infrastructure\Projection;

use BadMethodCallException;

class Projector
{
    private static $instance = null;

    public static function instance(): self
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }


    public function __clone()
    {
        throw new BadMethodCallException('Clone is not supported');
    }

    private $projections = [];

    public function register(array $projections)
    {
        foreach ($projections as $projection) {
            $this->projections[$projection->listensTo()] = $projection;
        }
    }

    public function project(array $events): void
    {
        foreach ($events as $event) {

            if (isset($this->projections[get_class($event)])) {
                $this->projections[get_class($event)]
                    ->project($event);
            }
        }
    }
}
