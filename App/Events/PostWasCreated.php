<?php

namespace App\Events;

use App\DomainEvent;

class PostWasCreated implements DomainEvent
{
    private $postId;
    private $title;
    private $content;

    /**
     * PostWasCreated constructor.
     *
     * @param $postId
     * @param $title
     * @param $content
     */
    public function __construct($postId, $title, $content)
    {
        $this->postId = $postId;
        $this->title = $title;
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function id()
    {
        return $this->postId;
    }

    /**
     * @return mixed
     */
    public function title()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function content()
    {
        return $this->content;
    }
}
