<?php


namespace App\Blog;


use App\DomainEvent;
use App\DomainEventPublisher;
use App\Infrastructure\Projection\Projector;

class AggregateRoot
{
    private array $recordedEvents = [];

    protected function recordApplyAndPublishThat(
        DomainEvent $domainEvent
    ): void {
        $this->recordThat($domainEvent);
        $this->applyThat($domainEvent);
        $this->publishThat($domainEvent);
        $this->syncThat($domainEvent);
    }

    protected function recordThat(DomainEvent $domainEvent): void
    {
        $this->recordedEvents[] = $domainEvent;
    }

    protected function applyThat(DomainEvent $domainEvent): void
    {
        $modifier = 'apply' . explode('\\', get_class($domainEvent))[1];

        $this->$modifier($domainEvent);
    }

    protected function publishThat(DomainEvent $domainEvent): void
    {
        DomainEventPublisher::instance()->publish($domainEvent);
    }

    public function recordedEvents(): array
    {
        return $this->recordedEvents;
    }

    public function clearEvents()
    {
        $this->recordedEvents = [];
    }

    public function syncThat(DomainEvent $domainEvent): void
    {
        Projector::instance()->project([$domainEvent]);
    }
}
