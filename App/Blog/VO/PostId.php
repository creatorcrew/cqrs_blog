<?php


namespace App\Blog\VO;


use Ramsey\Uuid\Uuid;

class PostId
{
    protected string $uuid;

    public function uuid(): string
    {
        return $this->uuid;
    }

    protected function __construct($uuid)
    {
        $this->uuid = $uuid;
    }

    public static function create(): self
    {
        return new self(Uuid::uuid4());
    }
}
