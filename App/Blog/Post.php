<?php


namespace App\Blog;

use App\Blog\VO\PostId;
use App\Events\PostWasCreated;

class Post extends AggregateRoot
{
    private PostId $id;
    private string $title;
    private string $content;


    private function __construct(PostId $id)
    {
        $this->id = $id;
    }

    public static function writeNewFrom($title, $content): void
    {
        $postId = PostId::create();

        $post = new static($postId);

        $post->recordApplyAndPublishThat(
            new PostWasCreated($postId, $title, $content)
        );
    }


    protected function applyPostWasCreated(
        PostWasCreated $event
    ) {
        $this->id      = $event->id();
        $this->title   = $event->title();
        $this->content = $event->content();

    }

}
